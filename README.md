# java_ee_site

Structure

javaeeru_env - docker image and additional files  
javaee.ru - project files  
temp - folder which used to store docker wildfly container to store logs and deployments artifacts


## Development process with docker

Wildfly folder mapping:   
`/opt/jboss/wildfly/standalone/deployments/` to `./temp/deployments/`  
`/opt/jboss/wildfly/standalone/log` to `./temp/log`

* invoke `docker-compose up --build -d ` inside `./javaeeru_env` - start docker environment 
* invoke `mvn install` inside `javaee.ru` - build project and store result ear to `./temp/deployments/`
* invoke `docker-compose down` inside `./javaeeru_env` - stop docker env
