package ru.javaee.ui.managedbeans;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named("testMB")
@SessionScoped
public class TestMB implements Serializable {

    private static final long serialVersionUID = 1413860354383624312L;

}
